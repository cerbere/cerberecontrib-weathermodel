from cerbere.feature.cgrid import Grid
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
    'ERA5',
    'era_5-copernicus__20000101.nc',
)


@pytest.fixture(scope='module')
def reader():
    return 'ERA5025'


@pytest.fixture(scope='module')
def feature_class():
    return Grid