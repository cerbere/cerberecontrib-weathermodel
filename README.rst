Welcome to this contrib for weather model data.

Installation
============

Requires:
  * cerbere

Examples
========

ERA5 in NetCDF format
---------------------

ERA5 in NetCDF format provide fields in two different resolution: 0.50 or 0.25
degree, depending on the field.

To open an ERA5 Dataset with 0.50 degree resolution subgrid, use
``ERA5050NCDataset`` class:

>>> dst = cerbere.open_dataset(
...    'ERA5050NCDataset',
...    /home/ref-ecmwf/ERA5/2018/01/era_5-copernicus__20180101.nc,
...    )

or as a feature (CylindricalGridTimeSeries):

>>> cube = cerbere.open_as_feature(
...    'CylindricalGridTimeSeries',
...    /home/ref-ecmwf/ERA5/2018/01/era_5-copernicus__20180101.nc,
...    'ERA5050NCDataset',
...    )


To open an ERA5 Dataset with 0.25 degree resolution subgrid, use
``ERA5025NCDataset`` class:

>>> dst = cerbere.open_dataset(
...    'ERA5025NCDataset',
...    /home/ref-ecmwf/ERA5/2018/01/era_5-copernicus__20180101.nc,
...    )

or as a feature (CylindricalGridTimeSeries):

>>> cube = cerbere.open_as_feature(
...    'CylindricalGridTimeSeries',
...    /home/ref-ecmwf/ERA5/2018/01/era_5-copernicus__20180101.nc,
...    'ERA5025NCDataset',
...    )

