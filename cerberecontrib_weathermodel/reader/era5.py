import re
from pathlib import Path
from typing import Union

import xarray as xr

from cerbere.reader.basereader import BaseReader


class ERA5(BaseReader):
    pattern: str = re.compile(r"^.*era_5-copernicus__[0-9]{8}.nc$")
    engine: str = "netcdf4"
    description: str = "Open an ERA5 Dataset with 0.50 or 0.25 degree resolution subgrid in Xarray"
    url: str = "https://link_to/your_backend/documentation"
    resolution: str = '025'

    @classmethod
    def open_dataset(cls,
                     filename_or_obj: Union[str, Path],
                     **kwargs):

        ds = super().open_dataset(filename_or_obj, **kwargs)

        lat_name = 'latitude{0}'.format(cls.resolution)
        lon_name = 'longitude{0}'.format(cls.resolution)

        ds = ds.rename(
            {
                lat_name: 'lat',
                lon_name: 'lon',
            }
        )

        # drop other resolution variables
        drop_dims = [_ for _ in ds.dims 
                     if _ not in ['time', 'lat', 'lon']]
        ds = ds.drop_dims(drop_dims)

        # TODO proper shift on zero meridian and index ?
        ds['lon'].values[ds['lon'] > 180] -= 360. 

        return ds


class ERA5050(ERA5):
    resolution = '050'


class ERA5025(ERA5):
    resolution = '025'

