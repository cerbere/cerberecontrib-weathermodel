import re
from pathlib import Path
from typing import Union

import xarray as xr
from cerbere.reader.basereader import BaseReader


class SODA(BaseReader):
    pattern: str = re.compile(r"^.*soda3.4.2_5dy_ocean_reg_[0-9]{4}_[0-9]{2}_[0-9]{2}.nc$")
    engine: str = "netcdf4"
    description: str = "Open an SODA Dataset t-cell or u-cell subgrid in Xarray"
    url: str = "https://link_to/your_backend/documentation"
    grid: str = 't'

    @classmethod
    def open_dataset(cls, filename_or_obj: Union[str, Path], **kwargs):
        ds = xr.open_dataset(filename_or_obj, **kwargs)
        ds.encoding['source'] = filename_or_obj
        ds.encoding['reader'] = cls.__class__.__name__

        lat_name = 'y{0}_ocean'.format(cls.grid)
        lon_name = 'x{0}_ocean'.format(cls.grid)

        ds = ds.rename_dims({lat_name: 'lat', lon_name: 'lon'})
        ds = ds.rename_vars({lat_name: 'lat', lon_name: 'lon'})

        # remove variables on a different grid
        skip_grid = {
            'u': 't',
            't': 'u'
        }[cls.grid]
        skip_dims = [
            'x{0}_ocean'.format(skip_grid),
            'y{0}_ocean'.format(skip_grid),
        ]
        ds = ds.drop_dims(skip_dims)

        if cls.grid == 't':
            ds = ds.drop_dims('sw_ocean')
        if cls.grid == 'u':
            ds = ds.drop_dims('s{0}_ocean'.format(skip_grid))

        # longitude convention
        attrs = ds['lon'].attrs
        encoding = ds['lon'].encoding
        ds = ds.reset_index('lon')
        lon = ds.lon.values
        lon[lon > 180] -= 360
        ds['lon'] = xr.DataArray(data=lon, dims='lon')
        # ds = ds.set_index({'lon': 'lon'}).reset_coords('lon', drop=True).set_coords('lon')
        # ds = ds.set_index({'lon': 'lon'})\
        #     .reset_coords()\
        #     .set_coords(['lon', 'lat', 'time'])
        ds['lon'].attrs = attrs
        ds['lon'].encoding = encoding

        # center on meridian 0
        ds = ds.roll(shifts={'lon': 360}, roll_coords=True)

        return ds


class SODAT(SODA):
    grid = 't'


class SODAU(SODA):
    grid = 'u'
